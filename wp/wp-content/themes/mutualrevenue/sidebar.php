<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<div id="primary" class="widget-area" role="complementary">
   <ul class="xoxo">

      <?php
      /* When we call the dynamic_sidebar() function, it'll spit out
       * the widgets for that widget area. If it instead returns false,
       * then the sidebar simply doesn't exist, so we'll hard-code in
       * some default sidebar stuff just in case.
       */
      if (!dynamic_sidebar('primary-widget-area')) :
         ?>

         <?php
         $pro = 
         '<li id = "providers" class = "widget-container widget_search">
         <span class = "widget-title">Service Providers</span>
         <a href="http://www.mutualrevenue.com/service-providers/" class="more">Read More</a>
         </li>';

         $pub = 
         '<li id = "publishers" class = "widget-container">
         <span class = "widget-title" > Publishers &amp; Affiliates</span>         
         <a href="http://www.mutualrevenue.com/publishers-affiliates/" class="more">Read More</a>
         </li>';

         $white = 
         '<li id = "whitelabel" class = "widget-container">
         <span class = "widget-title">White Label Partnership</span>
         <a href="http://www.mutualrevenue.com/white-label-partnership/" class="more">Read More</a>
         </li>';
         
         $contact = 
         '<a href="http://www.mutualrevenue.com/contact-us/" id="contact">Get in touch with us</a>';
         
         
            switch($post->ID)
            {
               case 17:
                  echo $pub.$white;
                  break;
               case 20:
                  echo $pro.$white;
                  break;
               case 26:
                  echo $pro.$pub;
                  break;
            }
            echo ($post->ID != 33? $contact: '');
         
         
         ?>

<?php endif; // end primary widget area  ?>
   </ul>
</div><!-- #primary .widget-area -->

<?php
// A second sidebar for widgets, just because.
if (is_active_sidebar('secondary-widget-area')) :
   ?>

   <div id="secondary" class="widget-area" role="complementary">
      <ul class="xoxo">
   <?php dynamic_sidebar('secondary-widget-area'); ?>
      </ul>
   </div><!-- #secondary .widget-area -->

<?php endif; ?>
