<?php
/**
 * Header Template
 *
 * The header template is generally used on every page of your site. Nearly all other
 * templates call it somewhere near the top of the file. It is used mostly as an opening
 * wrapper, which is closed with the footer.php file. It also executes key functions needed
 * by the theme, child themes, and plugins. 
 *
 * @package News
 * @subpackage Template
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
<title><?=get_post_meta($post->ID,"Page Title",true) ?></title>
<?php
if(substr($_SERVER['HTTP_HOST'],0,5)=='mail.'){
	echo '<meta name="robots" content="NOINDEX,NOFOLLOW">';
}?>
<meta name="description" content="<?=get_post_meta($post->ID,"Meta Description",true) ?>">
<link rel="stylesheet" href="/res/css/style.css" type="text/css" media="all" />
   <script type="text/javascript">WebFontConfig = {google: { families: [ 'Glegoo::latin' , 'Droid+Sans:400,700:latin', 'Advent+Pro:400,500,300:latin' ] }};(function() {var wf = document.createElement('script');wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';wf.type = 'text/javascript';wf.async = 'true';var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(wf, s);})(); </script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<script type="text/javascript">var _gaq=_gaq||[];_gaq.push(['_setAccount','UA-32509061-1']);_gaq.push(['_trackPageview']);(function(){var ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.src='//www.google-analytics.com/ga.js';var s0=document.getElementsByTagName('script')[0]; s0.parentNode.insertBefore(ga,s0)})();</script>

<script type="text/javascript">$(document).ready(function(){$("#featured > ul").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);});</script>
<?php //wp_head(); // WP head hook ?>

</head>

<body <?php body_class(); ?>>
<div id="wrapper">

   <header>
     <a href="http://www.mutualrevenue.com/"><img src="/images/logo.png" id="logo" /></a>
	
		<a href="http://reports.mutualrevenue.com/" class="submit login">Report Login</a>
	
	<ul id="users">
		<li style="border-right: solid 1px #4C8FD4;"><a href="http://www.mutualrevenue.com/contact-us/">Become an advertiser</a></li>
		<li style="border-left: solid 1px #84BCF6;"><a href="http://www.mutualrevenue.com/contact-us/">Become a publisher</a></li>
	</ul>
	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
   </header>
<?php if(is_home() || is_front_page()){ //only home banner ?>
   <div id="banner">
	
      <div class="services">
	<span class="title">Welcome to <h1 style="display: inline;font-size: 27px;">Mutual Revenue</h1></span>      
	<ul>
		<li style="border-top:0px;"><span>We develop clever solutions that help consumers find digital home services from cable, DSL and satellite providers where they live.</span></li>
		<li><span>We generate additional revenue for our affiliates and publishers who implement our comparison engines and availability checkers on their sites. You earn commission every time someone signs up for a service.</span></li>
                <li style="border-bottom:0px;"><span>We work with leading satellite, DSL and cable providers helping them generate more sales and leads.</span></li>
	</ul>
      </div>
   </div>
<div id="container">
      <ul id="boxes">
         <li style="margin-left: 6px;"><a href="http://mutualrevenue.com/wp/?page_id=17"><span class="title" style="color:#e9a815;">Service Providers</span><p>We work with cable, DSL and satellite TV, phone and internet providers to generate targeted leads and sales, and expose your brand to millions of highly targeted visitors. Add incremental high quality customers.</p><span class="more">Read More</span><div class="publish"></div></a></li>
         <li><a href="http://mutualrevenue.com/wp/?page_id=20"><span class="title">Publishers &amp; Affiliates</span><p>We offer affiliates the chance to earn generous commission from selling services from phone, TV and internet providers. We offer an unrivalled level of affiliate support and work hard with our affiliates to help them maximise the sales they generate.  </p><span class="more">Read More</span><div class="advertise"></div></a></li>
         <li style="margin-right: 0;"><a href="#"><span class="title" style="color:#66a9f0;">White label</span><p>Our white label comparison service provides engaging content for publisher sites. Our easy to implement solution could save your visitors hundreds of dollars a year on their home services & generate significant additional revenue from your website.</p><span class="more">Read More</span><div class="whitelabel"></div></a></li>
      </ul>
   </div>   
<?php } //end home banner ?>
